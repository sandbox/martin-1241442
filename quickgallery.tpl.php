<?php if (!$nsid || !$sets): ?>

<?php if ($nsid && !$sets): ?>
<p><em>Your Flickr identifier is set in your profile but you haven't put any
of your photographs into a Flickr photo set yet.</em></p>
<?php endif; ?>

<p>There are some <a href="/private/creating-galleries">video tutorials</a>
  which show you how to set up a Flickr account, upload some images, and organise
  them so that they can appear in a gallery on this web site,.</p>

<p>There are some requirements to setting up a gallery. See the
  <a href="/private/creating-galleries">video tutorials</a> for how get there.
  The requirements are:</p>
<ul>
<?php if (!$nsid): ?>
  <li>You must have a <a href="http://www.flickr.com">Flickr</a> account.</li>
<?php endif; ?>
  <li>You must have uploaded the images for the gallery to <a href="http://www.flickr.com">Flickr</a>.</li>
  <li>You must put the images you wish to display in your gallery in
  a flickr set.</li>
  <li>You can then create a gallery.</li>
</ul>
<?php if (!$nsid): ?>
<p>If you <a href="http://www.flickr.com/signup/" target="flickr">register your Flickr account</a> using the e-mail address
  <?php print $mail;?> your Flickr id will be set automatically.
  <?php print l('Click this link', $pageurl) ?> once you have created your
  Flickr account. If you use different e-mail address you will have to follow
  these instructions.</p>
<p>Once you have created a Flickr account you must add
  your Flickr identifier in your user profile.</p>
<p>To do this go to your
  <?php print l('profile', $profileurl, array('attributes' => array('target' => 'sccprofile'))) ?>
  (this will open in a new window or tab, close that when you are done) and
  find the Flickr settings section. Enter
  the e-mail address you have registered with Flickr as your Flickr identifier.</p>
<?php endif; ?>
<?php if ($sets): ?>
<p>You are now ready to <?php print l('create a gallery', $pageurl) ?></p>
<?php else: ?>
<p>Once you have added your photographs to a Flickr set you will be ready to
<?php print l('create a gallery', $pageurl) ?>.</p>
<?php endif; ?>
<?php endif; ?>
